# Módulo de Alumnos ![](images/icon.png)

 En este módulo se gestiona las asistencias de los alumnos a sus correspondiente materias.

 Una vez ejecutadio odoo debemos intalarlo para eso bucamos "mimodulo"

El modulo cuenta con un menu con tres opciones:
-   Alumno
-   Materia
-   Asiste

![](images/menuitem.png)

ALUMNOS
___
La primera opción es la de "Alumnos". Donde podremos crear los alumnos que estan asistiendo a la materias que se imparten.
![](images/creacionalumno.png)
Cuenta con diferentes campos:

El campo dni comprueba que la cantidad de caracteres introducidos es igual nueve , si no es asi devulve un mensaje indicando que faltan caracteres.

 Ademas el campo "Mayor de edad" comprueba la edad introducida previamente y se  marcara si es mayor de 18.

MATERIAS
___

 En sección de materias podemos introducir las materias que pueden cursar los alumnos, con una breve descripción de la materia.
 ![](images/creacionmaterias.png)

 ASISTENCIA
 ___

 Por último el la sección de "Asiste" donde se podra añadir la fecha en la que el alumnos ha asistido a determinada materia
 ![](images/creacionasiste.png)

____

El código del módulo cuenta con tres clases y sus respectivas vistas
- Alumno
 ![](images/codigoAlumno.png)

- Materia
 ![](images/codigoMaterias.png)

 - Asiste
 ![](images/codigoAsiste.png)
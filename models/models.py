# -*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp.exceptions import ValidationError

class Alumno(models.Model):
    _name = 'mimodulo.alumno'
    _rec_name = "nombre"
    #_inherits = {'res.users': 'user_id'}
    #user_id = fields.Many2one('res.users', ondelete='cascade')
    
    @api.depends('edad')
    def _check_edad_size(self):
        if self.edad >= 18:
            self.mayorEdad=True
        else:
            self.mayorEdad=False

    @api.constrains('dni')
    def _check_telefono_size(self):
        if len(self.dni) < 9:
            raise ValidationError('El campo dni debe tener nueve caracteres')

    dni = fields.Char(string="DNI:", size=9)
    nombre = fields.Char(string="Nombre:")
    apellidos = fields.Char(string="Apellidos:") 
    telefono = fields.Char(string="Telefono:", size=9)
    edad = fields.Integer(string="Edad:")
    mayorEdad=fields.Boolean('Mayor de edad',compute='_check_edad_size')
    pais = fields.Many2one('res.country',string="Pais:")
    asistencias_id = fields.One2many('mimodulo.asiste','alumno_id',string=" Asistencia Alumno:")



class Materias(models.Model):
    _name = 'mimodulo.materias'
    _rec_name = "nombre"

    nombre=fields.Selection([('sxe','SXE'),('ad','AD'),('eie','EIE'), ('pmdm','PMDM')],'Asignaturas:')
    descripcion = fields.Text(string="Descripcion:")


class Asiste(models.Model):
    _name = 'mimodulo.asiste'

    fecha = fields.Date(string="Fecha de asistencia:")
    alumno_id = fields.Many2one('mimodulo.alumno',string="Alumno:")
    materia_id = fields.Many2one('mimodulo.materias',string="Materia:")